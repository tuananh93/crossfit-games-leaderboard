#!/bin/bash

#Europe
region=29
country=France
rm -f result.txt
rm -rf $country
mkdir $country
curl https://c3po.crossfit.com/api/competitions/v2/competitions/quarterfinalsindividual/2022/leaderboards?division=1\&region=$region\&sort=0  >  result_men.txt
max_men_pages=$(grep -oP 'pagination\":\{\"totalPages\":\K\d+' result_men.txt)
for i in `seq 0 $max_men_pages`
do
	echo $i
	curl https://c3po.crossfit.com/api/competitions/v2/competitions/quarterfinalsindividual/2022/leaderboards?division=1\&region=$region\&sort=0\&page=$i  >>  result.txt
done

grep -oP 'firstNa[^{]*?lastName[^{]*'$country result.txt   | grep -oP 'firstName":"\K.*?(lastName":").*?"' | sed 's/\",\"lastName\":\"/ /g' | sed 's/\"//g'  > $country/men_tmp.txt
nl $country/men_tmp.txt > $country/men.txt

curl https://c3po.crossfit.com/api/competitions/v2/competitions/quarterfinalsindividual/2022/leaderboards?division=2\&region=$region\&sort=0  >  result_women.txt
max_men_pages=$(grep -oP 'pagination\":\{\"totalPages\":\K\d+' result_women.txt)
rm -f result.txt
for i in `seq 0 29`
do
	echo $i
	curl https://c3po.crossfit.com/api/competitions/v2/competitions/quarterfinalsindividual/2022/leaderboards?division=2\&region=$region\&sort=0\&page=$i  >>  result.txt
done

grep -oP 'firstNa[^{]*?lastName[^{]*'$country result.txt  | grep -oP 'firstName":"\K.*?(lastName":").*?"' | sed 's/\",\"lastName\":\"/ /g' | sed 's/\"//g'  > $country/women_tmp.txt
nl $country/women_tmp.txt > $country/women.txt

rm $country/*tmp.txt